#########################
# ifcopenshell setup.py #
#########################

from setuptools import setup, find_packages

setup(
    name="ifcopenshell",
    version="0.6.0",
    description="IfcOpenShell is an open source (LGPL) software library that helps users and software developers to work with the IFC file format.",
    packages=find_packages(),
    install_requires=[],
    python_requires=">=3.8",
)
