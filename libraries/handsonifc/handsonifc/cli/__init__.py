##################
# handsonifc.cli #
##################

import click

from .createIfc import createIfcCmd
from .greetings import greetingsCmd

# Introduce grouping.
@click.group()
def cli():
    """CLI"""

# Define "createIfc"
@cli.command("createIfc")
@click.option(
    "-p",
    "--path",
    default="data/inputs/",
    required=False,
    help="Specify path to input file(s).",
)
@click.option(
    "-d",
    "--destination",
    default="data/output.ifc",
    required=False,
    help="Specify full path to output file.",
)
@click.option(
    "-f",
    "--filename",
    default="handsonifc example",
    required=False,
    help="IFC file name.",
)
@click.option(
    "-c",
    "--creator",
    default="Anonymous",
    required=False,
    help="IFC file creator.",
)
@click.option(
    "-o",
    "--organization",
    default="ACME",
    required=False,
    help="Oganisational affiliation of creator.",
)
@click.option(
    "-n",
    "--project-name",
    default="handsonifc project",
    required=False,
    help="Specify name of project.",
)

@click.option(
    "-s",
    "--sidecar",
    default=".xlsx",
    required=False,
    help="Extension of sidecar file.",
)

@click.option(
    "-x",
    "--shift-x",
    default=0,
    required=False,
    help="Shift x-coordinate",
)

@click.option(
    "-y",
    "--shift-y",
    default=0,
    required=False,
    help="Shift y-coordinate",
)

@click.option(
    "-z",
    "--shift-z",
    default=0,
    required=False,
    help="Shift z-coordinate",
)

def createIfc(path, destination, filename, creator, organization, project_name, sidecar, shift_x, shift_y, shift_z):
    createIfcCmd(path, destination, filename, creator, organization, project_name, sidecar, shift_x, shift_y, shift_z)

# Define "greetings"
@cli.command("greetings")
@click.option(
    "-t",
    "--text",
    required=True,
)
def greetings(text):
    greetingsCmd(text)