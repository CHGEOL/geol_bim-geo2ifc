import json
from .base import SidecarReader


class JSONReader(SidecarReader):

    def _read(self, path):
        with open(path, 'r') as f:
            return json.loads(f.read())
